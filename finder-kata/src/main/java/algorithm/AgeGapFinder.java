package algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class AgeGapFinder {

  private final List<Person> people = new ArrayList<Person>();

  public AgeGapFinder(List<Person> people) {
    if (people != null) {
      this.people.addAll(people);
    }
  }

  public AgeGap find(SearchMode mode) {
    if (people.size() < 2) {
      return AgeGap.empty();
    }

    SortedSet<AgeGap> gaps = calculateAgeGaps();
    switch (mode) {
      case CLOSEST:
        return gaps.first();
      case FURTHEST:
      default:
        return gaps.last();
    }
  }

  private SortedSet<AgeGap> calculateAgeGaps() {
    final SortedSet<AgeGap> gaps = new TreeSet<AgeGap>();
    forEveryPairOfPeople(new PairOperation() {
      @Override
      public void execute(Person p1, Person p2) {
        gaps.add(AgeGap.fromComparison(p1, p2));
      }
    });
    return gaps;
  }

  private void forEveryPairOfPeople(PairOperation operation) {
    for (int i = 0; i < people.size() - 1; i++) {
      for (int j = i + 1; j < people.size(); j++) {
        operation.execute(people.get(i), people.get(j));
      }
    }
  }

  private interface PairOperation {

    void execute(Person p1, Person p2);

  }
}
