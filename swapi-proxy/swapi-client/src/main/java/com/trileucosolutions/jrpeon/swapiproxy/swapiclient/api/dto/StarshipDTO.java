package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StarshipDTO {

  private String url;

  private String name;

  @JsonProperty("max_atmosphering_speed")
  private String maxAtmospheringSpeed;

}
