package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ListResult<T> {

  private int count;

  private String next;

  private String previous;

  private List<T> results = new ArrayList<>();

}
