package com.trileucosolutions.jrpeon.swapiproxy.swapiclient;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.StarWarsAPI;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.error.SwapiErrorConverter;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
public class SwapiClientConfig {

  @Value("${swapi-proxy.swapi.url:http://localhost:8383}")
  private String url;

  @Bean
  public Retrofit retrofit() {
    return new Retrofit.Builder().baseUrl(appendSlash(url))
        .addConverterFactory(jacksonConverterFactory())
        .client(clientWithLogging())
        .build();
  }

  @Bean
  public StarWarsAPI api(Retrofit retrofit) {
    return retrofit.create(StarWarsAPI.class);
  }

  @Bean
  public SwapiErrorConverter errorConverter(Retrofit retrofit) {
    return new SwapiErrorConverter(retrofit);
  }

  @Bean
  public SwapiClient swapiClient(StarWarsAPI api, SwapiErrorConverter errorConverter) {
    return new SwapiClient(api, errorConverter);
  }

  private String appendSlash(String url) {
    return url.endsWith("/") ? url : (url + "/");
  }

  private OkHttpClient clientWithLogging() {
    return new OkHttpClient.Builder().addInterceptor(loggingInterceptor())
        .build();
  }

  private JacksonConverterFactory jacksonConverterFactory() {
    ObjectMapper jsonMapper = new ObjectMapper();
    jsonMapper.registerModule(new JavaTimeModule());
    jsonMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return JacksonConverterFactory.create(jsonMapper);
  }

  private Interceptor loggingInterceptor() {
    final Logger log = LoggerFactory.getLogger(StarWarsAPI.class);
    return chain -> {
      Request request = chain.request();
      log.info(String.format("Sending request %s", request.url()));
      Response response = chain.proceed(request);
      log.info(String.format("Received response for %s with code %s", response.request()
          .url(), response.code()));
      return response;
    };
  }

}
