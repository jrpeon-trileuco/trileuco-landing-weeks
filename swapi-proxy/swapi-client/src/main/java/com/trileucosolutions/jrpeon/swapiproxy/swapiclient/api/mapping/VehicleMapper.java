package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.AtmospheringSpeed;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.Vehicle;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.VehicleId;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.VehicleDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils.SwapiUtils;

public class VehicleMapper {

  public static Vehicle map(VehicleDTO dto) {
    String vehicleId = SwapiUtils.getIdFromResourceUrl(dto.getUrl());
    return Vehicle.builder()
        .id(VehicleId.create(vehicleId))
        .name(Name.create(dto.getName()))
        .maxAtmospheringSpeed(AtmospheringSpeed.create(dto.getMaxAtmospheringSpeed()))
        .build();
  }

}
