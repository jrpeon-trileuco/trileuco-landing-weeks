package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api;

import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.FilmDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.ListResult;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.PersonDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.PlanetDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.StarshipDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.VehicleDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface StarWarsAPI {

  String PEOPLE_URL = "people";
  String PLANET_URL = "planets/{id}";
  String FILM_URL = "films/{id}";
  String VEHICLE_URL = "vehicles/{id}";
  String STARSHIP_URL = "starships/{id}";

  @GET(PEOPLE_URL)
  Call<ListResult<PersonDTO>> getPeople(@Query("search") String search);

  @GET(PLANET_URL)
  Call<PlanetDTO> getPlanet(@Path("id") String id);

  @GET(FILM_URL)
  Call<FilmDTO> getFilm(@Path("id") String id);

  @GET(VEHICLE_URL)
  Call<VehicleDTO> getVehicle(@Path("id") String id);

  @GET(STARSHIP_URL)
  Call<StarshipDTO> getStarship(@Path("id") String id);

}
