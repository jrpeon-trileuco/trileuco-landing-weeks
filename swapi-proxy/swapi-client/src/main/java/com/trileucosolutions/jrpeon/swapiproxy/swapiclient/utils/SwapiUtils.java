package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SwapiUtils {

  private static final Pattern RESOURCE_URL_PATTERN = Pattern.compile("^.*/api/.*/(?<id>\\d+)(/)?$");

  public static String getIdFromResourceUrl(String url) {
    Matcher matcher = RESOURCE_URL_PATTERN.matcher(url);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Malformed resource url " + url);
    }
    return Optional.of(matcher.group("id"))
        .orElseThrow(() -> new IllegalArgumentException("Id not found on resource url " + url));
  }

}
