package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.FilmId;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.FilmDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils.SwapiUtils;

public class FilmMapper {

  public static Film map(FilmDTO dto) {
    String filmId = SwapiUtils.getIdFromResourceUrl(dto.getUrl());
    return Film.builder()
        .id(FilmId.create(filmId))
        .releaseDate(dto.getReleaseDate())
        .title(Name.create(dto.getTitle()))
        .build();
  }

}
