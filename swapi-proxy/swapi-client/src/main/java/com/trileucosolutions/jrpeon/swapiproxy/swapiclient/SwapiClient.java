package com.trileucosolutions.jrpeon.swapiproxy.swapiclient;

import com.trileucosolutions.jrpeon.swapiproxy.application.service.Swapi;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.SwapiErrorException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.FilmId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Person;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.Planet;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.PlanetId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.Starship;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.StarshipId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.Vehicle;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.VehicleId;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.StarWarsAPI;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.FilmDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.PersonDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.PlanetDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.StarshipDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.VehicleDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.error.SwapiError;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.error.SwapiErrorConverter;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping.FilmMapper;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping.PersonMapper;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping.PlanetMapper;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping.StarshipMapper;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping.VehicleMapper;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.exception.SwapiConnectionException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;

public class SwapiClient implements Swapi {

  private static final Logger LOG = LoggerFactory.getLogger(SwapiClient.class);

  private final StarWarsAPI api;

  private final SwapiErrorConverter errorConverter;

  public SwapiClient(StarWarsAPI api, SwapiErrorConverter errorConverter) {
    this.api = api;
    this.errorConverter = errorConverter;
  }

  @Override
  public List<Person> findPeopleByName(Name name) {
    List<PersonDTO> dtos = execute(api.getPeople(name.toString())).getResults();
    return PersonMapper.map(dtos);
  }

  @Override
  public Optional<Planet> findPlanet(PlanetId id) {
    Optional<PlanetDTO> dto  = executeOptional(api.getPlanet(id.toString()));
    return dto.map(PlanetMapper::map);
  }

  @Override
  public Optional<Film> findFilm(FilmId id) {
    Optional<FilmDTO> dto  = executeOptional(api.getFilm(id.toString()));
    return dto.map(FilmMapper::map);
  }

  @Override
  public Optional<Vehicle> findVehicle(VehicleId id) {
    Optional<VehicleDTO> dto  = executeOptional(api.getVehicle(id.toString()));
    return dto.map(VehicleMapper::map);
  }

  @Override
  public Optional<Starship> findStarship(StarshipId id) {
    Optional<StarshipDTO> dto  = executeOptional(api.getStarship(id.toString()));
    return dto.map(StarshipMapper::map);
  }

  private <R> Optional<R> executeOptional(Call<R> call) {
    R result;
    try {
      result = execute(call);
    } catch (SwapiErrorException error) {
      if (error.getCode() == 404) {
        return Optional.empty();
      } else {
        throw error;
      }
    }
    return Optional.of(result);
  }

  private <R> R execute(Call<R> call) {
    Response<R> response;
    try {
      response = call.execute();
    } catch (IOException e) {
      throw new SwapiConnectionException("Failed to connect to  SWAPI", e);
    }

    if (response.code() == 200) {
      return response.body();
    } else {
      if (response.errorBody() != null) {
        try {
          SwapiError swapiError = errorConverter.convert(response);
          throw new SwapiErrorException(response.code(), swapiError.getDetail());
        } catch (IOException e) {
          LOG.warn("Error trying to convert SWAPI error response", e);
        }
      }
      throw new SwapiConnectionException("Received HTTP code " + response.code());
    }
  }

}
