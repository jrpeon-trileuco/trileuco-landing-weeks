package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.error;

import java.io.IOException;
import java.lang.annotation.Annotation;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SwapiErrorConverter {

  private Converter<ResponseBody, SwapiError> converter;

  public SwapiErrorConverter(Retrofit retrofit) {
    this.converter = retrofit.responseBodyConverter(SwapiError.class, new Annotation[0]);
  }

  public SwapiError convert(Response<?> response) throws IOException {
    return converter.convert(response.errorBody());
  }

}
