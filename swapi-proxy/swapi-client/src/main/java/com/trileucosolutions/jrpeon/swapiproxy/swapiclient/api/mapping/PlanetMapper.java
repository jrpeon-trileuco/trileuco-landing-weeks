package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.Planet;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.PlanetId;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.PlanetDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils.SwapiUtils;

public class PlanetMapper {

  public static Planet map(PlanetDTO dto) {
    String planetId = SwapiUtils.getIdFromResourceUrl(dto.getUrl());
    return Planet.builder()
        .id(PlanetId.create(planetId))
        .name(Name.create(dto.getName()))
        .build();
  }

}
