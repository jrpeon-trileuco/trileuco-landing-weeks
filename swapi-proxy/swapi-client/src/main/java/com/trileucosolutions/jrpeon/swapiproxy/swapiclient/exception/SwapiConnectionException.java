package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.exception;

public class SwapiConnectionException extends RuntimeException {

  public SwapiConnectionException(String message) {
    super(message);
  }

  public SwapiConnectionException(String message, Throwable cause) {
    super(message, cause);
  }

}
