package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.StarWarsYear;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.FilmId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Gender;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Person;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.PersonId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.PlanetId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.StarshipId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.VehicleId;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.PersonDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils.SwapiUtils;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PersonMapper {

  public static Person map(PersonDTO dto) {
    String personId = SwapiUtils.getIdFromResourceUrl(dto.getUrl());
    String homeworldId = SwapiUtils.getIdFromResourceUrl(dto.getHomeworld());
    List<FilmId> filmIds = dto.getFilms()
        .stream()
        .map(SwapiUtils::getIdFromResourceUrl)
        .map(FilmId::create)
        .collect(Collectors.toList());
    List<VehicleId> vehicleIds = dto.getVehicles()
        .stream()
        .map(SwapiUtils::getIdFromResourceUrl)
        .map(VehicleId::create)
        .collect(Collectors.toList());
    List<StarshipId> starshipIds = dto.getStarships()
        .stream()
        .map(SwapiUtils::getIdFromResourceUrl)
        .map(StarshipId::create)
        .collect(Collectors.toList());
    return Person.builder()
        .id(PersonId.create(personId))
        .name(Name.create(dto.getName()))
        .birthYear(StarWarsYear.create(dto.getBirthYear()))
        .gender(Gender.create(dto.getGender()))
        .homeworldId(PlanetId.create(homeworldId))
        .filmIds(filmIds)
        .vehicleIds(vehicleIds)
        .starshipIds(starshipIds)
        .build();
  }

  public static List<Person> map(Collection<PersonDTO> dtos) {
    return dtos.stream()
        .map(PersonMapper::map)
        .collect(Collectors.toList());
  }

}
