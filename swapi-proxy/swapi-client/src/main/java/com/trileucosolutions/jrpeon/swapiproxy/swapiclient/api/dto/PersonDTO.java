package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PersonDTO {

  private String url;

  private String name;

  private String gender;

  @JsonProperty("birth_year")
  private String birthYear;

  private String homeworld;

  private List<String> films = new ArrayList<>();

  private List<String> starships = new ArrayList<>();

  private List<String> vehicles = new ArrayList<>();

}
