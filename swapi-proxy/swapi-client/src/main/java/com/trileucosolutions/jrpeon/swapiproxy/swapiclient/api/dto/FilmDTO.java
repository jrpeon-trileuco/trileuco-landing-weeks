package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FilmDTO {

  private String url;

  @JsonProperty("release_date")
  private LocalDate releaseDate;

  private String title;

}
