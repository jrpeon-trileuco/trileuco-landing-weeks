package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlanetDTO {

  private String url;

  private String name;

}
