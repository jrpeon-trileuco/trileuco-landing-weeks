package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.error;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SwapiError {

  private String detail;

}
