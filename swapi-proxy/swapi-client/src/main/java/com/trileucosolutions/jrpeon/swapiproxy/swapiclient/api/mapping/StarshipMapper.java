package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.AtmospheringSpeed;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.Starship;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.StarshipId;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.api.dto.StarshipDTO;
import com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils.SwapiUtils;

public class StarshipMapper {

  public static Starship map(StarshipDTO dto) {
    String starshipId = SwapiUtils.getIdFromResourceUrl(dto.getUrl());
    return Starship.builder()
        .id(StarshipId.create(starshipId))
        .name(Name.create(dto.getName()))
        .maxAtmospheringSpeed(AtmospheringSpeed.create(dto.getMaxAtmospheringSpeed()))
        .build();
  }

}
