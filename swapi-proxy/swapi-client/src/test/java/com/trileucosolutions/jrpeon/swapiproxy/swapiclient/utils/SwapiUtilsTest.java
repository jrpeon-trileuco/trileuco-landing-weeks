package com.trileucosolutions.jrpeon.swapiproxy.swapiclient.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class SwapiUtilsTest {

  @Test
  void getIdFromResourceUrl_givenValidUrl_shouldParseId() {
    String planetUrl = "http://swapi.trileuco.com:1138/api/planets/1/";

    String id = SwapiUtils.getIdFromResourceUrl(planetUrl);

    assertThat(id).isEqualTo("1");
  }

}
