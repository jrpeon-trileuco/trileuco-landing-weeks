package com.trileucosolutions.jrpeon.swapiproxy.domain.common;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class AtmospheringSpeedTest {

  @Test
  void create_givenValidSpeed_shouldParseValueCorrectly() {
    String value = "30";

    AtmospheringSpeed atms = AtmospheringSpeed.create(value);

    assertThat(atms).hasToString(value);
  }


}
