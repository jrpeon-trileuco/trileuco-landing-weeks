package com.trileucosolutions.jrpeon.swapiproxy.application.usecase;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import com.trileucosolutions.jrpeon.swapiproxy.application.service.Swapi;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.AtmospheringSpeed;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.StarWarsYear;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.NoMatchingResultsFound;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.FilmId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Gender;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Person;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.PersonId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.Planet;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.PlanetId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.Starship;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.StarshipId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.Vehicle;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.VehicleId;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FindPersonDetailsTest {

  private static final Name SEARCH_NAME = Name.create("Skywalker");

  private static final PlanetId PLANET_ID = PlanetId.create("1");

  @Mock
  private Swapi mockSwapi;

  private FindPersonDetails findPersonDetails;

  @BeforeEach
  void setUp() {
    findPersonDetails = new FindPersonDetails(mockSwapi);
  }

  @Nested
  class Execute {

    @Nested
    class GivenNameThatMatchesResults {

      @Test
      void shouldGatherDetailsAndReturn() {
        when(mockSwapi.findPeopleByName(SEARCH_NAME)).thenReturn(buildListOfPerson());
        when(mockSwapi.findPlanet(PLANET_ID)).thenReturn(Optional.of(buildPlanet()));
        when(mockSwapi.findFilm(FilmId.create("1"))).thenReturn(Optional.of(buildFilm1()));
        when(mockSwapi.findFilm(FilmId.create("2"))).thenReturn(Optional.of(buildFilm2()));
        when(mockSwapi.findFilm(FilmId.create("3"))).thenReturn(Optional.of(buildFilm3()));
        when(mockSwapi.findFilm(FilmId.create("6"))).thenReturn(Optional.of(buildFilm6()));
        when(mockSwapi.findVehicle(VehicleId.create("14"))).thenReturn(Optional.of(buildVehicle14()));
        when(mockSwapi.findVehicle(VehicleId.create("30"))).thenReturn(Optional.of(buildVehicle30()));
        when(mockSwapi.findStarship(StarshipId.create("12"))).thenReturn(Optional.of(buildStarship12()));
        when(mockSwapi.findStarship(StarshipId.create("22"))).thenReturn(Optional.of(buildStarship22()));

        PersonDetails personDetails = findPersonDetails.execute(SEARCH_NAME);

        assertThat(personDetails.getId()).isEqualTo(PersonId.create("1"));
        assertThat(personDetails.getName()).isEqualTo(Name.create("Luke Skywalker"));
        assertThat(personDetails.getBirthYear()).isEqualTo(StarWarsYear.create("19BBY"));
        assertThat(personDetails.getFastestVehicleDriven()).hasValue(Name.create("X-wing"));
        assertThat(personDetails.getGender()).isEqualTo(Gender.create("male"));
        assertThat(personDetails.getPlanetName()).hasValue(Name.create("Tatooine"));
        assertThat(personDetails.getFilms()).hasSize(4);
        assertThat(personDetails.getFilms().get(0)).satisfies(firstFilm -> {
          assertThat(firstFilm.getTitle()).isEqualTo(Name.create("A New Hope"));
          assertThat(firstFilm.getReleaseDate()).isEqualTo(LocalDate.parse("1977-05-25"));
        });
        assertThat(personDetails.getFilms().get(3)).satisfies(lastFilm -> {
          assertThat(lastFilm.getTitle()).isEqualTo(Name.create("Revenge of the Sith"));
          assertThat(lastFilm.getReleaseDate()).isEqualTo(LocalDate.parse("2005-05-19"));
        });
      }

    }

    @Nested
    class WhenNoResultsFound {

      @Test
      void shouldThrowException() {
        when(mockSwapi.findPeopleByName(SEARCH_NAME)).thenReturn(Collections.emptyList());

        NoMatchingResultsFound e = assertThrows(NoMatchingResultsFound.class, () ->
            findPersonDetails.execute(SEARCH_NAME));

        assertThat(e.getMessage()).isEqualTo("No results found matching 'name' with value 'Skywalker'");
      }

    }
  }

  private List<Person> buildListOfPerson() {
    Person luke = Person.builder()
        .id(PersonId.create("1"))
        .name(Name.create("Luke Skywalker"))
        .gender(Gender.create("male"))
        .homeworldId(PLANET_ID)
        .birthYear(StarWarsYear.create("19BBY"))
        .filmIds(Stream.of("1", "2", "3", "6")
            .map(FilmId::create)
            .collect(Collectors.toList()))
        .starshipIds(Stream.of("12", "22")
            .map(StarshipId::create)
            .collect(Collectors.toList()))
        .vehicleIds(Stream.of("14", "30")
            .map(VehicleId::create)
            .collect(Collectors.toList()))
        .build();
    Person anakin = Person.builder()
        .id(PersonId.create("11"))
        .name(Name.create("Anakin Skywalker"))
        .gender(Gender.create("male"))
        .homeworldId(PlanetId.create("1"))
        .birthYear(StarWarsYear.create("41.9BBY"))
        .filmIds(Stream.of("4", "5", "6")
            .map(FilmId::create)
            .collect(Collectors.toList()))
        .starshipIds(Stream.of("39", "59", "75")
            .map(StarshipId::create)
            .collect(Collectors.toList()))
        .vehicleIds(Stream.of("44", "46")
            .map(VehicleId::create)
            .collect(Collectors.toList()))
        .build();
    return Arrays.asList(luke, anakin);
  }

  private Planet buildPlanet() {
    return Planet.builder()
        .id(PLANET_ID)
        .name(Name.create("Tatooine"))
        .build();
  }

  private Film buildFilm1() {
    return Film.builder()
        .id(FilmId.create("1"))
        .title(Name.create("A New Hope"))
        .releaseDate(LocalDate.parse("1977-05-25"))
        .build();
  }

  private Film buildFilm2() {
    return Film.builder()
        .id(FilmId.create("2"))
        .title(Name.create("The Empire Strikes Back"))
        .releaseDate(LocalDate.parse("1980-05-17"))
        .build();
  }

  private Film buildFilm3() {
    return Film.builder()
        .id(FilmId.create("3"))
        .title(Name.create("Return of the Jedi"))
        .releaseDate(LocalDate.parse("1983-05-25"))
        .build();
  }


  private Film buildFilm6() {
    return Film.builder()
        .id(FilmId.create("6"))
        .title(Name.create("Revenge of the Sith"))
        .releaseDate(LocalDate.parse("2005-05-19"))
        .build();
  }

  private Vehicle buildVehicle14() {
    return Vehicle.builder()
        .id(VehicleId.create("14"))
        .name(Name.create("Snowspeeder"))
        .maxAtmospheringSpeed(AtmospheringSpeed.create("650"))
        .build();
  }

  private Vehicle buildVehicle30() {
    return Vehicle.builder()
        .id(VehicleId.create("30"))
        .name(Name.create("Imperial Speeder Bike"))
        .maxAtmospheringSpeed(AtmospheringSpeed.create("360"))
        .build();
  }

  private Starship buildStarship12() {
    return Starship.builder()
        .id(StarshipId.create("12"))
        .name(Name.create("X-wing"))
        .maxAtmospheringSpeed(AtmospheringSpeed.create("1050"))
        .build();
  }

  private Starship buildStarship22() {
    return Starship.builder()
        .id(StarshipId.create("22"))
        .name(Name.create("Imperial shuttle"))
        .maxAtmospheringSpeed(AtmospheringSpeed.create("850"))
        .build();
  }

}
