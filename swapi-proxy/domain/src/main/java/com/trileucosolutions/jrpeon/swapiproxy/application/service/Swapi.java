package com.trileucosolutions.jrpeon.swapiproxy.application.service;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.FilmId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Person;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.Planet;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.PlanetId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.Starship;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.StarshipId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.Vehicle;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.VehicleId;
import java.util.List;
import java.util.Optional;

public interface Swapi {

  List<Person> findPeopleByName(Name name);

  Optional<Planet> findPlanet(PlanetId id);

  Optional<Film> findFilm(FilmId id);

  Optional<Vehicle> findVehicle(VehicleId id);

  Optional<Starship> findStarship(StarshipId id);

}
