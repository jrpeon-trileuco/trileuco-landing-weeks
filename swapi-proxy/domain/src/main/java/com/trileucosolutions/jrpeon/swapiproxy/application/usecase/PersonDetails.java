package com.trileucosolutions.jrpeon.swapiproxy.application.usecase;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.StarWarsYear;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Gender;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.PersonId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PersonDetails {

  private final PersonId id;

  private final Name name;

  private final StarWarsYear birthYear;

  private final Gender gender;

  private final Name planetName;

  private final Name fastestVehicleDriven;

  private final List<Film> films = new ArrayList<>();

  public PersonDetails(PersonId id, Name name, StarWarsYear birthYear, Gender gender,
      Name planetName, Name fastestVehicleDriven, List<Film> films) {
    if (id == null) {
      throw new MandatoryParameterException("id");
    }
    if (name == null) {
      throw new MandatoryParameterException("name");
    }
    if (birthYear == null) {
      throw new MandatoryParameterException("birthYear");
    }
    if (gender == null) {
      throw new MandatoryParameterException("gender");
    }
    this.id = id;
    this.name = name;
    this.birthYear = birthYear;
    this.gender = gender;
    this.planetName = planetName;
    this.fastestVehicleDriven = fastestVehicleDriven;
    if (CollectionUtils.isNotEmpty(films)) {
      this.films.addAll(films);
      this.films.sort(Comparator.comparing(Film::getReleaseDate));
    }
  }

  public PersonId getId() {
    return id;
  }

  public Name getName() {
    return name;
  }

  public StarWarsYear getBirthYear() {
    return birthYear;
  }

  public Gender getGender() {
    return gender;
  }

  public Optional<Name> getPlanetName() {
    return Optional.ofNullable(planetName);
  }

  public Optional<Name> getFastestVehicleDriven() {
    return Optional.ofNullable(fastestVehicleDriven);
  }

  public List<Film> getFilms() {
    return Collections.unmodifiableList(films);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PersonDetails that = (PersonDetails) o;
    return new EqualsBuilder()
        .append(id, that.id)
        .append(name, that.name)
        .append(birthYear, that.birthYear)
        .append(gender, that.gender)
        .append(planetName, that.planetName)
        .append(fastestVehicleDriven, that.fastestVehicleDriven)
        .append(films, that.films)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(id)
        .append(name)
        .append(birthYear)
        .append(gender)
        .append(planetName)
        .append(fastestVehicleDriven)
        .append(films)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .append("name", name)
        .append("birthYear", birthYear)
        .append("gender", gender)
        .append("planetName", planetName)
        .append("fastestVehicleDriven", fastestVehicleDriven)
        .append("films", films)
        .toString();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private PersonId id;

    private Name name;

    private StarWarsYear birthYear;

    private Gender gender;

    private Name planetName;

    private Name fastestVehicleDriven;

    private List<Film> films = new ArrayList<>();

    private Builder() {
    }

    public Builder id(PersonId id) {
      this.id = id;
      return this;
    }

    public Builder name(Name name) {
      this.name = name;
      return this;
    }

    public Builder birthYear(StarWarsYear birthYear) {
      this.birthYear = birthYear;
      return this;
    }

    public Builder gender(Gender gender) {
      this.gender = gender;
      return this;
    }

    public Builder planetName(Name planetName) {
      this.planetName = planetName;
      return this;
    }

    public Builder fastestVehicleDriven(Name fastestVehicleDriven) {
      this.fastestVehicleDriven = fastestVehicleDriven;
      return this;
    }

    public Builder films(List<Film> films) {
      if (CollectionUtils.isNotEmpty(films)) {
        this.films.clear();
        this.films.addAll(films);
      }
      return this;
    }

    public PersonDetails build() {
      return new PersonDetails(id, name, birthYear, gender, planetName, fastestVehicleDriven, films);
    }
  }
}