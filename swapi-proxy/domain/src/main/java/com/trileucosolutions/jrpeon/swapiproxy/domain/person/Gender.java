package com.trileucosolutions.jrpeon.swapiproxy.domain.person;

import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Gender {

  private final String value;

  private Gender(String value) {
    if (StringUtils.isBlank(value)) {
      throw new MandatoryParameterException("value");
    }
    this.value = value;
  }

  public static Gender create(String value) {
    return new Gender(value);
  }

  public String value() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Gender gender = (Gender) o;
    return new EqualsBuilder()
        .append(value, gender.value)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(value)
        .toHashCode();
  }

}
