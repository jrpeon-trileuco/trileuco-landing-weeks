package com.trileucosolutions.jrpeon.swapiproxy.domain.exception;

public class SwapiErrorException extends RuntimeException {

  private int code;

  public SwapiErrorException(int code, String detail) {
    super(detail);
    this.code = code;
  }

  public int getCode() {
    return code;
  }

}
