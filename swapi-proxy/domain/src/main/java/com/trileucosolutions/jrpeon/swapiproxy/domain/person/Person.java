package com.trileucosolutions.jrpeon.swapiproxy.domain.person;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.StarWarsYear;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.FilmId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.PlanetId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.StarshipId;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.VehicleId;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Person {

  private final PersonId id;

  private final Name name;

  private final StarWarsYear birthYear;

  private final Gender gender;

  private final PlanetId homeworldId;

  private final Set<FilmId> filmIds = new HashSet<>();

  private final Set<VehicleId> vehicleIds = new HashSet<>();

  private final Set<StarshipId> starshipIds = new HashSet<>();

  private Person(PersonId id, Name name, StarWarsYear birthYear, Gender gender,
      PlanetId homeworldId, Collection<FilmId> filmIds, Collection<VehicleId> vehicleIds,
      Collection<StarshipId> starshipIds) {
    if (id == null) {
      throw new MandatoryParameterException("id");
    }
    if (name == null) {
      throw new MandatoryParameterException("name");
    }
    if (birthYear == null) {
      throw new MandatoryParameterException("birthYear");
    }
    if (gender == null) {
      throw new MandatoryParameterException("gender");
    }
    if (homeworldId == null) {
      throw new MandatoryParameterException("homeworldId");
    }
    this.id = id;
    this.name = name;
    this.birthYear = birthYear;
    this.gender = gender;
    this.homeworldId = homeworldId;
    if (CollectionUtils.isNotEmpty(filmIds)) {
      this.filmIds.addAll(filmIds);
    }
    if (CollectionUtils.isNotEmpty(vehicleIds)) {
      this.vehicleIds.addAll(vehicleIds);
    }
    if (CollectionUtils.isNotEmpty(starshipIds)) {
      this.starshipIds.addAll(starshipIds);
    }
  }

  public PersonId getId() {
    return id;
  }

  public Name getName() {
    return name;
  }

  public StarWarsYear getBirthYear() {
    return birthYear;
  }

  public Gender getGender() {
    return gender;
  }

  public PlanetId getHomeworldId() {
    return homeworldId;
  }

  public boolean starredInAnyFilms() {
    return !this.filmIds.isEmpty();
  }

  public boolean starredIn(FilmId filmId) {
    return this.filmIds.contains(filmId);
  }

  public Set<FilmId> getFilmIds() {
    return Collections.unmodifiableSet(filmIds);
  }

  public boolean pilotedAnyVehicles() {
    return !this.vehicleIds.isEmpty();
  }

  public boolean canPilot(VehicleId vehicleId) {
    return this.vehicleIds.contains(vehicleId);
  }

  public Set<VehicleId> getVehicleIds() {
    return Collections.unmodifiableSet(vehicleIds);
  }

  public boolean canPilot(StarshipId starshipId) {
    return this.starshipIds.contains(starshipId);
  }

  public boolean pilotedAnyStarships() {
    return !this.starshipIds.isEmpty();
  }

  public Set<StarshipId> getStarshipIds() {
    return Collections.unmodifiableSet(starshipIds);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Person person = (Person) o;
    return new EqualsBuilder()
        .append(id, person.id)
        .append(name, person.name)
        .append(birthYear, person.birthYear)
        .append(gender, person.gender)
        .append(homeworldId, person.homeworldId)
        .append(filmIds, person.filmIds)
        .append(vehicleIds, person.vehicleIds)
        .append(starshipIds, person.starshipIds)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(id)
        .append(name)
        .append(birthYear)
        .append(gender)
        .append(homeworldId)
        .append(filmIds)
        .append(vehicleIds)
        .append(starshipIds)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .append("name", name)
        .append("birthYear", birthYear)
        .append("gender", gender)
        .append("homeworldId", homeworldId)
        .append("filmIds", filmIds)
        .append("vehicleIds", vehicleIds)
        .append("starshipIds", starshipIds)
        .toString();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private PersonId id;

    private Name name;

    private StarWarsYear birthYear;

    private Gender gender;

    private PlanetId homeworldId;

    private Set<FilmId> filmIds = new HashSet<>();

    private Set<VehicleId> vehicleIds = new HashSet<>();

    private Set<StarshipId> starshipIds = new HashSet<>();

    private Builder() {
    }

    public Builder id(PersonId id) {
      this.id = id;
      return this;
    }

    public Builder name(Name name) {
      this.name = name;
      return this;
    }

    public Builder birthYear(StarWarsYear birthYear) {
      this.birthYear = birthYear;
      return this;
    }

    public Builder gender(Gender gender) {
      this.gender = gender;
      return this;
    }

    public Builder homeworldId(PlanetId homeworldId) {
      this.homeworldId = homeworldId;
      return this;
    }

    public Builder filmIds(Collection<FilmId> filmIds) {
      this.filmIds.clear();
      this.filmIds.addAll(filmIds);
      return this;
    }

    public Builder vehicleIds(Collection<VehicleId> vehicleIds) {
      this.vehicleIds.clear();
      this.vehicleIds.addAll(vehicleIds);
      return this;
    }

    public Builder starshipIds(Collection<StarshipId> starshipIds) {
      this.starshipIds.clear();
      this.starshipIds.addAll(starshipIds);
      return this;
    }

    public Person build() {
      return new Person(id, name, birthYear, gender, homeworldId, filmIds, vehicleIds, starshipIds);
    }
  }
}
