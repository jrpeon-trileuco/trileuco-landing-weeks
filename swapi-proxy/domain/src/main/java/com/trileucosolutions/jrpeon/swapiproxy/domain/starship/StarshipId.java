package com.trileucosolutions.jrpeon.swapiproxy.domain.starship;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Id;

public class StarshipId extends Id {

  private StarshipId(String value) {
    super(value);
  }

  public static StarshipId create(String value) {
    return new StarshipId(value);
  }

}
