package com.trileucosolutions.jrpeon.swapiproxy.domain.person;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Id;

public class PersonId extends Id {

  private PersonId(String value) {
    super(value);
  }

  public static PersonId create(String value) {
    return new PersonId(value);
  }

}
