package com.trileucosolutions.jrpeon.swapiproxy.domain.planet;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Planet {

  private final PlanetId id;

  private final Name name;

  public Planet(PlanetId id, Name name) {
    if (id == null) {
      throw new MandatoryParameterException("id");
    }
    if (name == null) {
      throw new MandatoryParameterException("name");
    }
    this.id = id;
    this.name = name;
  }

  public PlanetId getId() {
    return id;
  }

  public Name getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Planet planet = (Planet) o;
    return new EqualsBuilder()
        .append(id, planet.id)
        .append(name, planet.name)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(id)
        .append(name)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .append("name", name)
        .toString();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private PlanetId id;

    private Name name;

    private Builder() {
    }

    public Builder id(PlanetId id) {
      this.id = id;
      return this;
    }

    public Builder name(Name name) {
      this.name = name;
      return this;
    }

    public Planet build() {
      return new Planet(id, name);
    }
  }
}
