package com.trileucosolutions.jrpeon.swapiproxy.domain.common;

import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.InvalidParameterException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class AtmospheringSpeed implements Comparable<AtmospheringSpeed> {

  private static final Pattern PATTERN = Pattern.compile("^(?<value>\\d+)(km)?$");

  private static final String NOT_APPLICABLE_VALUE = "n/a";

  public static final AtmospheringSpeed NOT_APPLICABLE = new AtmospheringSpeed(NOT_APPLICABLE_VALUE);

  private final String value;

  private AtmospheringSpeed(String value) {
    if (StringUtils.isBlank(value)) {
      throw new MandatoryParameterException("value");
    }
    if (value.equals(NOT_APPLICABLE_VALUE)) {
      this.value = value;
    } else {
      Matcher matcher = PATTERN.matcher(value);
      if (!matcher.matches()) {
        throw new InvalidParameterException("value",
            "atmosphering speed value '" + value + "' does not match '" + PATTERN.pattern() + "' and is not 'n/a'");
      }
      this.value = matcher.group("value");
    }
  }

  public static AtmospheringSpeed create(String value) {
    if (StringUtils.equals(value, NOT_APPLICABLE_VALUE)) {
      return NOT_APPLICABLE;
    } else {
      return new AtmospheringSpeed(value);
    }
  }

  public String value() {
    return value;
  }

  public int intValue() {
    return value.equals(NOT_APPLICABLE_VALUE) ? 0 : Integer.parseInt(value);
  }

  @Override
  public int compareTo(AtmospheringSpeed o) {
    return Integer.compare(this.intValue(), o.intValue());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AtmospheringSpeed that = (AtmospheringSpeed) o;
    return new EqualsBuilder()
        .append(value, that.value)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(value)
        .toHashCode();
  }

  @Override
  public String toString() {
    return value;
  }

}
