package com.trileucosolutions.jrpeon.swapiproxy.domain.exception;

public class InvalidParameterException extends RuntimeException {

  private static final String MESSAGE_PATTERN = "Invalid '%s', '%s'";

  public InvalidParameterException(String parameterName, String details) {
    super(String.format(MESSAGE_PATTERN, parameterName, details));
  }


}
