package com.trileucosolutions.jrpeon.swapiproxy.domain.film;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import java.time.LocalDate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Film {

  private final FilmId id;

  private final LocalDate releaseDate;

  private final Name title;

  public Film(FilmId id, LocalDate releaseDate, Name title) {
    if (id == null) {
      throw new MandatoryParameterException("id");
    }
    if (releaseDate == null) {
      throw new MandatoryParameterException("releaseDate");
    }
    if (title == null) {
      throw new MandatoryParameterException("title");
    }
    this.id = id;
    this.releaseDate = releaseDate;
    this.title = title;
  }

  public FilmId getId() {
    return id;
  }

  public LocalDate getReleaseDate() {
    return releaseDate;
  }

  public Name getTitle() {
    return title;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Film film = (Film) o;
    return new EqualsBuilder()
        .append(id, film.id)
        .append(releaseDate, film.releaseDate)
        .append(title, film.title)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(id)
        .append(releaseDate)
        .append(title)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .append("releaseDate", releaseDate)
        .append("title", title)
        .toString();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private FilmId id;

    private LocalDate releaseDate;

    private Name title;

    private Builder() {
    }

    public Builder id(FilmId id) {
      this.id = id;
      return this;
    }

    public Builder releaseDate(LocalDate releaseDate) {
      this.releaseDate = releaseDate;
      return this;
    }

    public Builder title(Name title) {
      this.title = title;
      return this;
    }

    public Film build() {
      return new Film(id, releaseDate, title);
    }
  }
}
