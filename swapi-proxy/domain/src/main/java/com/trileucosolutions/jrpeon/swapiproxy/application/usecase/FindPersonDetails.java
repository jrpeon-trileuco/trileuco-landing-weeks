package com.trileucosolutions.jrpeon.swapiproxy.application.usecase;

import com.trileucosolutions.jrpeon.swapiproxy.application.service.Swapi;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.NoMatchingResultsFound;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import com.trileucosolutions.jrpeon.swapiproxy.domain.person.Person;
import com.trileucosolutions.jrpeon.swapiproxy.domain.piloted.Piloted;
import com.trileucosolutions.jrpeon.swapiproxy.domain.planet.Planet;
import com.trileucosolutions.jrpeon.swapiproxy.domain.starship.Starship;
import com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle.Vehicle;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;

@Component
public class FindPersonDetails {

  private final Swapi swapi;

  public FindPersonDetails(Swapi swapi) {
    this.swapi = swapi;
  }

  public PersonDetails execute(Name name) {
    List<Person> peopleWithMatchingNames = swapi.findPeopleByName(name);
    Person person = peopleWithMatchingNames.stream()
        .findFirst()
        .orElseThrow(() -> new NoMatchingResultsFound("name", name.toString()));

    Optional<Planet> homeworld = swapi.findPlanet(person.getHomeworldId());
    Optional<? extends Piloted> fastestVehicleDriven = findFastestVehicle(person);
    List<Film> films = findFilms(person);

    return PersonDetails.builder()
        .id(person.getId())
        .name(person.getName())
        .birthYear(person.getBirthYear())
        .gender(person.getGender())
        .planetName(homeworld.map(Planet::getName)
            .orElse(null))
        .fastestVehicleDriven(fastestVehicleDriven.map(Piloted::getName)
            .orElse(null))
        .films(films)
        .build();
  }

  private Optional<? extends Piloted> findFastestVehicle(Person person) {
    return Stream.of(findVehicles(person), findStarships(person))
        .flatMap(List::stream)
        .max(Comparator.comparing(Piloted::getMaxAtmospheringSpeed));
  }

  private List<Film> findFilms(Person person) {
    return person.getFilmIds()
        .stream()
        .map(swapi::findFilm)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());
  }

  private List<Vehicle> findVehicles(Person person) {
    return person.getVehicleIds()
        .stream()
        .map(swapi::findVehicle)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());
  }

  private List<Starship> findStarships(Person person) {
    return person.getStarshipIds()
        .stream()
        .map(swapi::findStarship)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());
  }
}
