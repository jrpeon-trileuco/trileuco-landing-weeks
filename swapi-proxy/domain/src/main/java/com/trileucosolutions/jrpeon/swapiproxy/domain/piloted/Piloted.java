package com.trileucosolutions.jrpeon.swapiproxy.domain.piloted;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.AtmospheringSpeed;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;

public interface Piloted {

  Name getName();

  AtmospheringSpeed getMaxAtmospheringSpeed();

}
