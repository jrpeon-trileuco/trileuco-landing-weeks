package com.trileucosolutions.jrpeon.swapiproxy.domain.exception;

public class NoMatchingResultsFound extends RuntimeException {

  private static final String MESSAGE_PATTERN = "No results found matching '%s' with value '%s'";

  public NoMatchingResultsFound(String paramName, String value) {
    super(String.format(MESSAGE_PATTERN, paramName, value));
  }

}
