package com.trileucosolutions.jrpeon.swapiproxy.domain.exception;

public class MandatoryParameterException extends RuntimeException {

  private static final String MESSAGE_PATTERN = "Argument '%s' is mandatory";

  public MandatoryParameterException(String parameterName) {
    super(String.format(MESSAGE_PATTERN, parameterName));
  }

}
