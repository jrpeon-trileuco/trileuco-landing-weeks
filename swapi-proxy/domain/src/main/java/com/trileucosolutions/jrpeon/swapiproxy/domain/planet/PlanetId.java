package com.trileucosolutions.jrpeon.swapiproxy.domain.planet;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Id;

public class PlanetId extends Id {

  private PlanetId(String value) {
    super(value);
  }

  public static PlanetId create(String value) {
    return new PlanetId(value);
  }

}
