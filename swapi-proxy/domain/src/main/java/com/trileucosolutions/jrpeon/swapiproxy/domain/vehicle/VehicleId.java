package com.trileucosolutions.jrpeon.swapiproxy.domain.vehicle;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Id;

public class VehicleId extends Id {

  private VehicleId(String value) {
    super(value);
  }

  public static VehicleId create(String value) {
    return new VehicleId(value);
  }

}
