package com.trileucosolutions.jrpeon.swapiproxy.domain.film;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Id;

public class FilmId extends Id {

  private FilmId(String value) {
    super(value);
  }

  public static FilmId create(String value) {
    return new FilmId(value);
  }

}
