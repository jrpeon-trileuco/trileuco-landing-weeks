package com.trileucosolutions.jrpeon.swapiproxy.domain.starship;

import com.trileucosolutions.jrpeon.swapiproxy.domain.common.AtmospheringSpeed;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.piloted.Piloted;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Starship implements Piloted {

  private final StarshipId id;

  private final Name name;

  private final AtmospheringSpeed maxAtmospheringSpeed;

  public Starship(StarshipId id, Name name, AtmospheringSpeed maxAtmospheringSpeed) {
    if (id == null) {
      throw new MandatoryParameterException("id");
    }
    if (name == null) {
      throw new MandatoryParameterException("name");
    }
    if (maxAtmospheringSpeed == null) {
      throw new MandatoryParameterException("maxAtmospheringSpeed");
    }
    this.id = id;
    this.name = name;
    this.maxAtmospheringSpeed = maxAtmospheringSpeed;
  }

  public StarshipId getId() {
    return id;
  }

  @Override
  public Name getName() {
    return name;
  }

  @Override
  public AtmospheringSpeed getMaxAtmospheringSpeed() {
    return maxAtmospheringSpeed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Starship starship = (Starship) o;
    return new EqualsBuilder()
        .append(id, starship.id)
        .append(name, starship.name)
        .append(maxAtmospheringSpeed, starship.maxAtmospheringSpeed)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(id)
        .append(name)
        .append(maxAtmospheringSpeed)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .append("name", name)
        .append("maxAtmospheringSpeed", maxAtmospheringSpeed)
        .toString();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private StarshipId id;

    private Name name;

    private AtmospheringSpeed maxAtmospheringSpeed;

    private Builder() {
    }

    public Builder id(StarshipId id) {
      this.id = id;
      return this;
    }

    public Builder name(Name name) {
      this.name = name;
      return this;
    }

    public Builder maxAtmospheringSpeed(AtmospheringSpeed maxAtmospheringSpeed) {
      this.maxAtmospheringSpeed = maxAtmospheringSpeed;
      return this;
    }

    public Starship build() {
      return new Starship(id, name, maxAtmospheringSpeed);
    }
  }
}
