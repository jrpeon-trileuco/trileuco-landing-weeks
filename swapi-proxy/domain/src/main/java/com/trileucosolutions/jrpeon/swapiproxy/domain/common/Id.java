package com.trileucosolutions.jrpeon.swapiproxy.domain.common;

import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class Id {

  private final String value;

  protected Id(String value) {
    if (StringUtils.isBlank(value)) {
      throw new MandatoryParameterException(value);
    }
    this.value = value;
  }

  public String value() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Id id = (Id) o;
    return new EqualsBuilder()
        .append(value, id.value)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(value)
        .toHashCode();
  }
}
