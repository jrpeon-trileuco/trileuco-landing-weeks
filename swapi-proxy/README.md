# Swapi Proxy

A simple SpringBoot-based proxy for https://swapi.co/

## Getting Started

### Prerequisites

You'll need:
* JDK 8+
* Gradle 6

### Installing

1. Build the project

    ```
    $ gradle build
    ```
    This will create both a executable .jar in `/build/libs` and compiled code for local execution.
   
2. Start the app
    ```
    $ gradle :boot:bootRun
    ``` 

    API will be avaible at `http://localhost:8080`. Root will display Swagger documentation.


## Running the tests

Use
```
$ gradle test
``` 
