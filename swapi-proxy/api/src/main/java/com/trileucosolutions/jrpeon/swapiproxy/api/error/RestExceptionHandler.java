package com.trileucosolutions.jrpeon.swapiproxy.api.error;

import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.InvalidParameterException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.MandatoryParameterException;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.NoMatchingResultsFound;
import com.trileucosolutions.jrpeon.swapiproxy.domain.exception.SwapiErrorException;
import java.util.Optional;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class RestExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

  @ExceptionHandler({NoMatchingResultsFound.class})
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  public ApiError handleNotFoundExceptions(Exception exception) {
    LOG.debug(exception.getMessage(), exception);
    return ApiError.builder()
        .code(HttpStatus.NOT_FOUND.value())
        .detail(exception.getMessage())
        .build();
  }

  @ExceptionHandler({InvalidParameterException.class, MandatoryParameterException.class, ConstraintViolationException.class,
      HttpMediaTypeException.class, MethodArgumentTypeMismatchException.class, HttpRequestMethodNotSupportedException.class,
      HttpMessageNotReadableException.class, ServletRequestBindingException.class, MethodArgumentNotValidException.class})
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleBadRequestExceptions(Exception exception) {
    LOG.debug(exception.getMessage(), exception);
    return ApiError.builder()
        .code(HttpStatus.BAD_REQUEST.value())
        .detail(exception.getMessage())
        .build();
  }

  @ExceptionHandler({SwapiErrorException.class})
  @ResponseBody
  public ResponseEntity<ApiError> handleSwapiExceptions(SwapiErrorException exception) {
    LOG.debug(exception.getMessage(), exception);
    ApiError error = ApiError.builder()
        .code(exception.getCode())
        .detail(exception.getMessage())
        .build();
    HttpStatus statusCode = Optional.ofNullable(HttpStatus.resolve(exception.getCode()))
        .orElse(HttpStatus.CONFLICT);
    return new ResponseEntity<>(error, statusCode);
  }

  @ExceptionHandler({Exception.class})
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ApiError handleUnknownExceptions(Exception exception) {
    LOG.error(exception.getMessage(), exception);
    return ApiError.builder()
        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
        .detail("Encountered unexpected error")
        .build();
  }

}
