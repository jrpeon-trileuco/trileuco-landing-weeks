package com.trileucosolutions.jrpeon.swapiproxy.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.api.dto.PersonDetailsDTO;
import com.trileucosolutions.jrpeon.swapiproxy.application.usecase.PersonDetails;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;

public class PersonDetailsMapper {

  public static PersonDetailsDTO map(PersonDetails person) {
    return PersonDetailsDTO.builder()
        .name(person.getName().toString())
        .birthYear(person.getBirthYear().toString())
        .fastestVehicleDriven(person.getFastestVehicleDriven()
            .map(Name::toString)
            .orElse(null))
        .gender(person.getName().toString())
        .planetName(person.getPlanetName()
            .map(Name::toString)
            .orElse(null))
        .films(FilmMapper.map(person.getFilms()))
        .build();
  }

}
