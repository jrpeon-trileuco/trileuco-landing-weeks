package com.trileucosolutions.jrpeon.swapiproxy.api.mapping;

import com.trileucosolutions.jrpeon.swapiproxy.api.dto.FilmDTO;
import com.trileucosolutions.jrpeon.swapiproxy.domain.film.Film;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FilmMapper {

  public static FilmDTO map(Film film) {
    return FilmDTO.builder()
        .name(film.getTitle().toString())
        .releaseDate(film.getReleaseDate())
        .build();
  }

  public static List<FilmDTO> map(Collection<Film> films) {
    return films.stream()
        .map(FilmMapper::map)
        .collect(Collectors.toList());
  }

}
