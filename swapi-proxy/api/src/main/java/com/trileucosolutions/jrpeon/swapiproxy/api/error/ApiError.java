package com.trileucosolutions.jrpeon.swapiproxy.api.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ApiError {

  private Integer code;

  private String detail;

}
