package com.trileucosolutions.jrpeon.swapiproxy.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class PersonDetailsDTO {

  private String name;

  @JsonProperty("birth_year")
  private String birthYear;

  private String gender;

  @JsonProperty("planet_name")
  private String planetName;

  @JsonProperty("fastest_vehicle_driven")
  private String fastestVehicleDriven;

  private List<FilmDTO> films;

}