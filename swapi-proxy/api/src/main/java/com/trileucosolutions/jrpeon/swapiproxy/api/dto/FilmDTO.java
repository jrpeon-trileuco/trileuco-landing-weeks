package com.trileucosolutions.jrpeon.swapiproxy.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class FilmDTO {

  private String name;

  @JsonProperty("release_date")
  private LocalDate releaseDate;

}
