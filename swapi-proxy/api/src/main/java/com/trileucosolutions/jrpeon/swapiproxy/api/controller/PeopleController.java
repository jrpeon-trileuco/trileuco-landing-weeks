package com.trileucosolutions.jrpeon.swapiproxy.api.controller;

import static com.trileucosolutions.jrpeon.swapiproxy.api.mapping.PersonDetailsMapper.map;

import com.trileucosolutions.jrpeon.swapiproxy.api.dto.PersonDetailsDTO;
import com.trileucosolutions.jrpeon.swapiproxy.application.usecase.FindPersonDetails;
import com.trileucosolutions.jrpeon.swapiproxy.domain.common.Name;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/swapi-proxy", produces = "application/json")
public class PeopleController {

  private FindPersonDetails findPersonDetails;

  public PeopleController(FindPersonDetails findPersonDetails) {
    this.findPersonDetails = findPersonDetails;
  }

  @GetMapping(path = "/person-info")
  public PersonDetailsDTO findPersonDetails(@RequestParam String name) {
    return map(findPersonDetails.execute(Name.create(name)));
  }

}
