import java.util.Objects;

public class Player {

  private final String name;

  private int score = 0;

  private Player(String name) {
    Objects.requireNonNull(name);
    this.name = name;
  }

  public static Player create(String name) {
    return new Player(name);
  }

  public String getName() {
    return name;
  }

  public int getScore() {
    return score;
  }

  public boolean sameScoreAs(Player other) {
    return this.score == other.score;
  }

  public int scoreDifference(Player other) {
    return Math.abs(this.score - other.score);
  }

  public boolean isAheadOf(Player other) {
    return this.score > other.score;
  }

  public boolean hasLessThanFourPoints() {
    return this.score < 4;
  }

  public void resetScore() {
    this.score = 0;
  }

  public void addPoint() {
    this.score++;
  }
}
