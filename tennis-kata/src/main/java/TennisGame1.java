import java.util.Objects;

public class TennisGame1 implements TennisGame {

    private String player1Name;
    private int player1Score = 0;
    private String player2Name;
    private int player2Score = 0;

    public TennisGame1(String player1Name, String player2Name) {
        Objects.requireNonNull(player1Name);
        Objects.requireNonNull(player2Name);
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public void wonPoint(String playerName) {
        Objects.requireNonNull(playerName);
        if (playerName.equals("player1")) {
            player1Score++;
        } else {
            player2Score++;
        }
    }

    public String getScore() {
        if (player1Score == player2Score) {
            return tyingScoreName(player1Score);
        }
        if (player1Score >=4 || player2Score >=4) {
            return endgameScoreName(player1Score, player2Score);
        }
        return bothPlayerScoreName(player1Score, player2Score);
    }

    private String bothPlayerScoreName(int player1Score, int player2Score) {
        return scoreName(player1Score)
            + "-"
            + scoreName(player2Score);
    }

    private static String scoreName(int score) {
        switch(score) {
            case 0:
                return "Love";
            case 1:
                return "Fifteen";
            case 2:
                return "Thirty";
            case 3:
            default:
                return "Forty";
        }
    }

    private static String endgameScoreName(int player1Score, int player2Score) {
        int difference = player1Score - player2Score;
        if (difference == 1) {
            return "Advantage player1";
        }
        if (difference == -1) {
            return "Advantage player2";
        }
        if (difference >= 2) {
            return "Win for player1";
        }
        return "Win for player2";
    }

    private static String tyingScoreName(int score) {
        switch (score) {
            case 0:
                return "Love-All";
            case 1:
                return "Fifteen-All";
            case 2:
                return "Thirty-All";
            default:
                return "Deuce";
        }
    }

    @Override
    public void resetGame() {
        this.player1Score = 0;
        this.player2Score = 0;
    }
}
