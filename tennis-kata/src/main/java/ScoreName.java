import java.util.stream.Stream;

public enum ScoreName {
  LOVE(0),
  FIFTEEN(1),
  THIRTY(2),
  FORTY(3);

  int value;

  ScoreName(int value) {
    this.value = value;
  }

  public static ScoreName fromScore(int value) {
    return Stream.of(ScoreName.values())
        .filter(scoreName -> scoreName.value == value)
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("No score name for value " + value));
  }

  public String capitalised() {
    String thisName = this.toString();
    return thisName.substring(0, 1).toUpperCase() + thisName.substring(1).toLowerCase();
  }

}
